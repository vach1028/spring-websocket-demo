package cn.sjd.config;

import cn.sjd.handler.HandShake;
import cn.sjd.handler.TextMessageHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

import javax.annotation.Resource;

/**

 * WebScoket配置处理器

 */
@Component
@EnableWebSocket
public class WebSocketConfig extends WebMvcConfigurerAdapter implements WebSocketConfigurer {

    @Autowired
    private TextMessageHandler textMessageHandler;

    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        registry.addHandler(textMessageHandler, "/ws").addInterceptors(new HandShake()).setAllowedOrigins("*");

        registry.addHandler(textMessageHandler, "/ws/sockjs").addInterceptors(new HandShake()).withSockJS();
    }

}