package cn.sjd.controller;

import cn.sjd.model.WiselyMessage;
import cn.sjd.model.WiselyResponse;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

/**
 * Created by Eastern unbeaten on 2017/2/8.
 */
@Controller
public class WebController {
    @MessageMapping("/welcome")//1
    @SendTo("/topic/getResponse")//2
    public WiselyResponse say(WiselyMessage message){
        return new WiselyResponse("Welcome, " + message.getName() + "!");
    }
}
